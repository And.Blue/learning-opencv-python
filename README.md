Learning OpenCV and Testing an Architecture
======


# Introduction 
This little project is to test out OpenCV, and a POC about how to design an App for _flexible_ image processing relying on OpenCV 

# Getting Started
## 1. Installation process

This project requires python3 installed on your computer.
The recommended way to execute this project is to launch it through a python3 virtual environment in order to avoid
dependency/language versions conflicts.
To do so :
- open up the terminal and navigate to the root of the project's directory.
- run `python -m venv venv` to install in this directory the virtual environment in within the /venv folder
- run `source venv/bin/activate` to activate the virtual environment (this command is tested on Linux and MacOS, but may vary on Windows).
## 2. Software dependencies

This project requires OpenCV, Numpy and part of CV-CommonTools. make sure that you have [pip](https://pip.pypa.io/en/stable/) installed.
- run `pip install -r requirements.txt` to install dependencies.
at the moment CV-CommonTools is not yet packaged.
- run `pip install dist\\gfi_cv_common_tools-1.2.7.tar` to install the dependencies.

# Build and Test
- run them through idea or in commandline by typing  `python -m unittest {name of module, for example:  simple_math_with_callbacks}` 
