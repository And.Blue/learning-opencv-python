import unittest

from utils import file_with_suffix

"""
    --------------------------------------------------------------------------------------------------
    Unit tests
    --------------------------------------------------------------------------------------------------
    run them through idea or in commandline by typing  python -m 'unittest .' 
"""

if __name__ == '__main__':
    unittest.main()


class TestUtils(unittest.TestCase):

    def test_file_with_suffix_valid_no_subdir(self):
        filename = 'bob_to_this.jpg'
        suffix = 'GREYSCALE'
        self.assertEqual(file_with_suffix(filename, suffix, False), 'bob_to_this_GREYSCALE.jpg')

    def test_file_with_suffix_invalid_no_subdir(self):
        filename = 'ImAWrongFile'
        suffix = 'GREYSCALE'
        with self.assertRaises(ValueError):
            file_with_suffix(filename, suffix, False)

    def test_file_with_suffix_valid_with_subdir(self):
        filename = 'images/bob_to_this.jpg'
        suffix = 'GREYSCALE'
        self.assertEqual(file_with_suffix(filename, suffix), 'bob_to_this_GREYSCALE.jpg')

    def test_file_with_suffix_valid_with_subdir2(self):
        filename = 'mysubproject/images/bob_to_this.jpg'
        suffix = 'GREYSCALE'
        self.assertEqual(file_with_suffix(filename, suffix), 'bob_to_this_GREYSCALE.jpg')

    def test_file_with_suffix_valid_with_subdir3(self):
        filename = '/images/bob_to_this.jpg'
        suffix = 'GREYSCALE'
        self.assertEqual(file_with_suffix(filename, suffix), 'bob_to_this_GREYSCALE.jpg')

    def test_file_with_suffix_invalid_with_subdir(self):
        filename = 'images/ImAWrongFile'
        suffix = 'GREYSCALE'
        with self.assertRaises(ValueError):
            file_with_suffix(filename, suffix)
