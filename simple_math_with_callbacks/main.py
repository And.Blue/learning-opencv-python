from simple_math_with_callbacks.model import AnObject
from simple_math_with_callbacks.operators import add
from simple_math_with_callbacks.utils import obj_to_string
from simple_math_with_callbacks.callbacks import multiply_by_minus_4_callback, add_10_callback, remove_20_callback, \
    multiply_by_2_callback
from typing import List, Callable


def apply_operations(an_object: AnObject, callbacks: List[Callable[[AnObject], None]]):
    for callback in callbacks:
        callback(an_object)


if __name__ == '__main__':
    obj = AnObject(3)
    print(f'Before operations \n =============== \n {obj_to_string(obj)} \n ===============')
    apply_operations(obj, [multiply_by_minus_4_callback, add_10_callback, remove_20_callback, multiply_by_2_callback])
    print(f'After operations \n =============== \n {obj_to_string(obj)} \n ===============')

    add(obj, 2)

    obj_to_string(obj)
