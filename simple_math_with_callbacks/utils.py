from simple_math_with_callbacks.model import AnObject

"""
    Helpers used to avoid repetitive code
"""


def obj_to_string(obj: AnObject) -> str:
    """
    Prints image_out the
    :param obj:
    :return: NoReturn
    """
    return f'OBJ -> value: {obj.number} , number of operations applied to value : {obj.count}'
