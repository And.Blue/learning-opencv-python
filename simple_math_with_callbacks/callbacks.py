from simple_math_with_callbacks.model import AnObject
from simple_math_with_callbacks.operators import multiply, add

"""
    Callbacks used in main : using the operators
"""


def add_2_callback(obj: AnObject) -> None:
    print("Add Two CallBack")
    add(obj, 2)


def add_10_callback(obj: AnObject) -> None:
    print("Add Ten Call Back")
    add(obj, 10)


def remove_20_callback(obj: AnObject) -> None:
    print("Remove 20 callback")
    add(obj, -20)


def multiply_by_2_callback(obj: AnObject) -> None:
    print("Multiply by 2 callback")
    multiply(obj, 2)


def multiply_by_minus_4_callback(obj: AnObject) -> None:
    print("Multiply by -4 callback")
    multiply(obj, -4)


