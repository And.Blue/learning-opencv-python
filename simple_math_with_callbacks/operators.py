from simple_math_with_callbacks.model import AnObject
from typing import NoReturn


def add(obj: AnObject, by=int) -> None:
    """
    :param obj: Object to manipulate
    :param by: value used for the operation
    :return: Returns nothing.
    """
    obj.number += by


def multiply(obj: AnObject, by=int) -> None:
    """
        Same as add()
    """
    obj.number *= by
