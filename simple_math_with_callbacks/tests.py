import unittest

from simple_math_with_callbacks.operators import add, multiply
from simple_math_with_callbacks.model import AnObject
from simple_math_with_callbacks.callbacks import multiply_by_2_callback, multiply_by_minus_4_callback, remove_20_callback, add_2_callback, \
    add_10_callback
from simple_math_with_callbacks.main import apply_operations

"""
    --------------------------------------------------------------------------------------------------
    Unit tests
    --------------------------------------------------------------------------------------------------
    run them through idea or in commandline by typing  python -m 'unittest simple_math_with_callbacks' 
"""


if __name__ == '__main__':
    unittest.main()


class TestApplyOperators(unittest.TestCase):

    def test_add_2_multiply_by_2_to_1(self):
        obj = AnObject(1)
        apply_operations(obj, [add_2_callback, multiply_by_2_callback])
        self.assertEqual(obj.number, 6)

    def test_add_10_to_2(self):
        obj = AnObject(2)
        apply_operations(obj, [add_10_callback])
        self.assertEqual(obj.number, 12)

    def test_add_2_multiply_by_2_multiply_by_minus_4_remove_20_to_3(self):
        obj = AnObject(3)
        apply_operations(obj, [add_2_callback, multiply_by_2_callback, multiply_by_minus_4_callback, remove_20_callback])
        self.assertEqual(obj.number, -60)


class TestOperators(unittest.TestCase):

    def test_add_2_to_1(self):
        obj = AnObject(1)
        add(obj, 2)
        self.assertEqual(obj.number, 3)

    def test_add_minus_10_to_12(self):
        obj = AnObject(12)
        add(obj, -10)
        self.assertEqual(obj.number, 2)

    def test_multiply_2_by_3(self):
        obj = AnObject(2)
        multiply(obj, 3)
        self.assertEqual(obj.number, 6)

    def test_multiply_30_by_minus_4(self):
        obj = AnObject(30)
        multiply(obj, -4)
        self.assertEqual(obj.number, -120)
