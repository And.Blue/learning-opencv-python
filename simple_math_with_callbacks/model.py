class AnObject:
    """
    Structure used to show how to use callbacks and decorators
    """
    def __init__(self, number: int):
        self._count = 0
        self._value = number

    @property
    def count(self) -> int:
        return self._count

    @property
    def number(self) -> int:
        return self._value

    @number.setter
    def number(self, new_number: int):
        self._count += 1
        self._value = new_number
