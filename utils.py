def file_with_suffix(file_path: str, suffix: str, in_sub_dir=True) -> str:
    """
    Adds a suffix to the filename and returns the value
    :param in_sub_dir: if file is placed in a sub directory ex: '/img/my_img.jpg'
    :param file_path:
    :param suffix: add a word/suffix to the end of the filename
    :return: file name with the suffix
    """

    if in_sub_dir:
        path_content = file_path.split('/')[-1]
    else:
        path_content = file_path

    file_content = path_content.split('.')

    if len(file_content) < 2:
        raise ValueError('The inputed file name isn\'t valid')

    return f'{file_content[0]}_{suffix}.{file_content[1]}'
