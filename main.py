from model import ImageToManipulate
from image_scripts import apply_greyscale, apply_gaussian, apply_edge_detection, apply_rescale, Operation, Argument
from typing import Tuple, Optional, List

import os


def apply_operations(image: ImageToManipulate, operations: List[Tuple[Operation, Optional[Argument]]]) -> None:
    """
    Applies methods/scripts to the Image.

    Operations to apply are defined in the dictionary passed
    The number of methods/scripts isn't fixed but each method  must fulfill the Operation callback format.
    The arguments of these methods are optional, and should be tuples if used by the method, or None if unused if
    the method doesn't need extra arguments.


    :param image: Image object that the operations will manipulate
    :param operations: List of methods with their arguments(as a single Tuple)
    :return: No return
    """
    for operation in operations:
        method = operation[0]
        argument = operation[1]

        if argument:
            method(image, argument)
        else:
            method(image)

    print("Finished applying operations.")


if __name__ == '__main__':
    img_path = 'image_in/yoshi/image_1.png'

    if os.path.isfile(img_path):
        img = ImageToManipulate(img_path, 'image_out/')
        # creates the temporary image that is going to be manipulated
        img.create_tmp()

        # (apply_greyscale, None),
        # (apply_edge_detection, (10, 200))

        apply_operations(image=img, operations=[
            (apply_rescale, (300, 300)),
            (apply_greyscale, None)

        ])



    else:
        print(f'The file {img_path} does not exist')
