from model import ImageToManipulate

import cv2
from typing import Tuple


def apply_rescale(img: ImageToManipulate, size: Tuple[int, int]):
    img_to_scale = img.read()
    resized_img = cv2.resize(img_to_scale, size)
    img.write(resized_img, f'RESCALED{size[0]}x{size[1]}')


