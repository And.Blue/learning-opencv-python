from model import ImageToManipulate
from numpy import hstack
from utils import file_with_suffix
import cv2
from typing import Callable, Optional, Tuple

"""
    
"""
# type used by these methods.
Argument = Tuple[int, Optional[int]]  # argument of operations is usually a tuple
Operation = Callable[[ImageToManipulate, Optional[Argument]], None]


def apply_greyscale(img: ImageToManipulate):
    img_to_manipulate = img.read()
    image_with_greyscale = cv2.cvtColor(img_to_manipulate, cv2.COLOR_BGR2GRAY)
    img.write(image_with_greyscale, f'GREYSCALE')


def apply_gaussian(img: ImageToManipulate):
    img_to_manipulate = img.read()
    with_gauss = cv2.GaussianBlur(img_to_manipulate, (5, 5), cv2.BORDER_DEFAULT)
    img.write(with_gauss, f'GAUSSIANBLUR')


def apply_edge_detection(img: ImageToManipulate, param: Tuple[int, int]):
    """
    :param img:
    :param param: ex: (10, 200)
    :return:
    """
    img_to_manipulate = img.read()
    with_edges = cv2.Canny(img_to_manipulate, param[0], param[1])
    img.write(with_edges, f'WITHEDGE')
