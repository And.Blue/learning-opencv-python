import cv2

import os
from utils import file_with_suffix


class ImageToManipulate:
    def __init__(self, img_path: str, output_dir: str = ''):
        self.img_path = img_path
        self.tmp_path = ''
        self.output_dir = output_dir

    def create_tmp(self):
        """
        Copies the image to the output file so it can be manipulated without destroying the original one
        :return:
        """
        img = cv2.imread(self.img_path)
        img_output_file = self.output_dir + file_with_suffix(self.img_path, '')
        cv2.imwrite(img_output_file, img)
        # we wish to manipulate the temp path
        self.img_path = img_output_file

    def read(self):
        """
        :return: Image read through OpenCV (cv2.imread(image_path))
        """
        return cv2.imread(self.img_path)

    def write(self, image_to_write, with_suffix: str = '', keep_and_use_old_image: bool = False):
        """
        :param keep_and_use_old_image Whether the old image is kept in directory and used for future operations
        if False, it automatically deletes the old image (the one used for the operation) and sets the new image
        as the current path.
        :param image_to_write Image to replace
        :param with_suffix:
        :return:
        """
        img_output_file = self.output_dir + file_with_suffix(self.img_path, with_suffix)
        cv2.imwrite(img_output_file, image_to_write)
        if not keep_and_use_old_image:
            self._dismiss_old_image(img_output_file)

    def _dismiss_old_image(self, new_image_path: str):
        """
        Method to run after operation,
        Keeps the post operation image and not the pre-operation image
        Deletes the temp image and switches to the path of the new post operation image
        :return:
        """
        os.remove(self.img_path)
        self.img_path = new_image_path
